package main

import (
	"sync"
	"fmt"
)

func main() {
	var waitGroup sync.WaitGroup
	waitGroup.Add(2)

	go func(){
		fmt.Println("one")
		waitGroup.Done()
	}()

	go func(){
		fmt.Println("two")
		waitGroup.Done()
	}()

	waitGroup.Wait()
}
