package main

import "fmt"

func main() {
	ch := make(chan int)

	go func() {
		fmt.Println("wait....")
		ch <- 1
	}()

	fmt.Println("do some thing...")
	v := <-ch
	println(v)
}

