package main

import (
	"fmt"
)

func main() {
	//var ch chan int
	ch := make(chan int)
	end := make(chan int,2)

	go func() {
		fmt.Println("new goroutine")
		ch <- 1
	}()

	go func(){
		chNum := <- ch
		fmt.Println("go routine2 ", chNum)
		end <- 2
	}()

	<-end
	fmt.Println("end")

}


